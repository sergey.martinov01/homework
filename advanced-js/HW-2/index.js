// Try...catch уместно использовать когда есть возможность
// возникновения ошибок в выполнении определенного участка кода
// таких как:
//  неправильные входные данные, сбой сетевого
// соединения, ошибка при работе с базой данных и другие.

const books = [
    {
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70,
    },
    {
      author: "Скотт Бэккер",
      name: "Воин-пророк",
    },
    {
      name: "Тысячекратная мысль",
      price: 70,
    },
    {
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70,
    },
    {
      author: "Дарья Донцова",
      name: "Детектив на диете",
      price: 40,
    },
    {
      author: "Дарья Донцова",
      name: "Дед Снегур и Морозочка",
    },
  ];
  
  const root = document.querySelector("#root");
  
  function createList(arr, container) {
    container.appendChild(createListFromArr(arr, container));
  }
  
function createListFromArr(arr, container) {
	const ul = document.createElement("ul");

    for (let elem of arr) {
      try {
				const mandatoryKeys = ["author", "name", "price"];
				
        for (let key of mandatoryKeys) {
          if (typeof elem[key] === "undefined") {
            throw new Error(`${key}`);
          }
        }
        container.appendChild(createListFromObject(elem));
      } catch (e) {
        console.log(`Error: ${e.message} is not defined`);
      }
    }
  
    return ul;
  }
  
  function createListFromObject(obj) {
    const ul = document.createElement("ul");
    for (let key in obj) {
      const li = document.createElement("li");
      li.innerHTML = `${key}: ${obj[key]}`;
      ul.appendChild(li);
    }
    return ul;
  }
  
  createList(books, root);
  