'use strict';
const burger = document.querySelector('.burger');
const nav = document.querySelector('.nav');
burger.addEventListener('click', () =>{
    burger.classList.toggle('active');
    nav.classList.toggle('active');
});


const footerContent = document.querySelector('.footer-content');

window.addEventListener('resize', changeFooterContent);


function changeFooterContent() {
    const clientWidth = document.documentElement.clientWidth;

    if (clientWidth < 767){
        footerContent.textContent = 'Maecenas faucibus molli interdum. Cras mattis consectetur purus sitor amet sed donec malesuada ullamcorper odio.'
    }
    if(clientWidth >= 767){
        footerContent.textContent = 'Maecenas faucibus molli interdum. Cras mattis consectetur purus sitor amet sed donec malesuada ullamcorper odio. Curabitur blandit tempus porttitor vollisky inceptos mollisestor.'
    }
};
changeFooterContent()