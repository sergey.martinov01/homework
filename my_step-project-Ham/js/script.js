// our services section


const tabs = document.querySelector(".tabs")
const tabsContent = document.querySelector(".tabs-content");
for (let i = 0; i < tabsContent.children.length; i++){
    // tabs.children[i].dataset.index = i;
    // tabsContent.children[i].dataset.index = i;

    if(!tabs.children[i].classList.contains("active")){
    tabsContent.children[i].hidden = true;
    }
}

tabs.addEventListener('click', (e) => {
    if(e.target.classList.contains('tabs-title') && !e.target.classList.contains('active')){
        const curentActiveTitle = tabs.querySelector('.active');
        const curentActiveIndex = [...tabs.children].indexOf(curentActiveTitle)
        const curentActiveContent =  tabsContent.children[curentActiveIndex];
        curentActiveTitle.classList.remove('active')
        curentActiveContent.hidden = true;

        const newActiveIndex = [...tabs.children].indexOf(e.target);
        const newActiveContent = tabsContent.children[newActiveIndex];
        newActiveContent.hidden = false;
        e.target.classList.add("active");
    }
})


// our amazing work section.



const tabsOurAmazingWork = document.querySelector(".tabs_Our-Amazing-Work")
const tabsContentOurAmazingWork = document.querySelector(".container_Our-Amazing-Work");
for (let i = 0; i < tabsContentOurAmazingWork.children.length; i++){
    // tabs.children[i].dataset.index = i;
    // tabsContent.children[i].dataset.index = i;

    if(!tabsOurAmazingWork.children[i].classList.contains("active-2")){
        tabsContentOurAmazingWork.children[i].hidden = true;
    }
}

tabsOurAmazingWork.addEventListener('click', (e) => {
    if(e.target.classList.contains('tabs-title_Our-Amazing-Work') && !e.target.classList.contains('active-2')){
        const curentActiveTitle2 = tabsOurAmazingWork.querySelector('.active-2');
        const curentActiveIndex2 = [...tabsOurAmazingWork.children].indexOf(curentActiveTitle2)
        const curentActiveContent2 =  tabsContentOurAmazingWork.children[curentActiveIndex2];
        curentActiveTitle2.classList.remove('active-2')
        curentActiveContent2.hidden = true;

        const newActiveIndex2 = [...tabsOurAmazingWork.children].indexOf(e.target);
        const newActiveContent2 = tabsContentOurAmazingWork.children[newActiveIndex2];
        newActiveContent2.hidden = false;
        e.target.classList.add("active-2");
    }
})

// load more imgs

const btnLoader = document.getElementById("load-btn");
let imgHidden = document.querySelectorAll(".hidden");
let imgHidden2 = document.querySelectorAll(".hidden2");

function loader() {
    const Preloader = document.querySelector('.preloader')
    Preloader.style.display = 'block';
    btnLoader.style.display = 'none';

    setTimeout(function(){
        btnLoader.style.display = 'flex';
        Preloader.style.display = 'none';

        if (!btnLoader.classList.contains('second-click')){
            for (let i = 0; i < imgHidden.length; i++){
                imgHidden[i].classList.toggle("hidden");
            }
            btnLoader.classList.add('second-click')
        }
        else {
            for (let i = 0; i < imgHidden2.length; i++){
                imgHidden2[i].classList.toggle("hidden2");
            }
            document.getElementById("load-btn").remove()
        }
    }, 2000)
}

btnLoader.onclick = loader;


// slider



$(document).ready(function(){
    $('.slider').slick({
        dots: true,
        arrows: true,
        speed: 800,
    });
})


