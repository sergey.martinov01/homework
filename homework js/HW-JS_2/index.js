"use strict";

// теоретический вопрос:
// циклы необходимы в програмировании,
// чтобы выполнять многократно определенный кусок кода, пока нам это нужно.

let number = +prompt("enter your number!");
if (number > 5) {
  for (let i = 0; i <= number; i += 5) {
    console.log(i);
  }
} else {
  console.log("Sorry no numbers");
}
