"use strict";

const num1 = +prompt("Enter first number");
const num2 = +prompt("Enter second number");
const userOperator = prompt("enter operator");
function calcResult(value1, value2, operator) {
  switch (operator) {
    case "+":
      return value1 + value2;
    case "-":
      return value1 - value2;
    case "*":
      return value1 * value2;
    case "/":
      if (value2 !== 0) {
        return value1 / value2;
      } else {
        alert("You cannot divide by zero!");
      }
  }
}
const result = calcResult(num1, num2, userOperator);
console.log(result);
