
const point = $('.scroll-menu');
const targets = $('.scroll-target');
const btnToUp = $('.btn-to-top');
const height = document.documentElement.clientHeight;
point.on('click', goToPoint);
btnToUp.on('click', () => slowScroll(0));

function goToPoint(event){
    const position = event.target;
    let top = 0;

    for (let target of targets){
        if ($(position).hasClass(`${target.id}`)){
            top = $(`#${target.id}`).offset().top
        }
    }
    slowScroll(top)
};

function slowScroll (position){
    $('html, body').animate({
        scrollTop: position
    }, 1000)
};

function showButtonToUp(){
    $(window).on('scroll', function(){
        if ($(window).scrollTop() >= 300){
            btnToUp.fadeIn();
        } else {
            btnToUp.fadeOut();
        }
    });
};
showButtonToUp();

function slideToggle(){
    $('.slide-toggle').click(function(){
        $('.container-background2').slideToggle({
            duration: 700,
            easing: 'swing'
        });
        if ($('.slide-toggle').text() === 'Скрыть раздел'){
            $('.slide-toggle').text('Вернуть раздел')
        } else {
            $('.slide-toggle').text('Скрыть раздел')
        }
    })
};
slideToggle();