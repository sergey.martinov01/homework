// forEach выполняет функцию к каждому эллементу массива в котором он вызван

const myArray = ['hello', 'world', 23, '23', null];

function filterBy(arr, type) {
    const filterArr = arr.filter( item => typeof item != type );
    return filterArr;
}
console.log(filterBy(myArray, "string"));