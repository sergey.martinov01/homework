const button = document.querySelector('.change-btn');
const link = document.querySelector('.style-link');

let flag = false;
let style = 'css/style.css';

if (localStorage.getItem('change-style')) {
    style = localStorage.getItem('change-style');
    flag = true;
}

link.setAttribute('href', style);

button.addEventListener('click', (e) => {
    localStorage.clear();
    if (!flag) {
        link.setAttribute('href', 'css/change-style.css');
        localStorage.setItem('change-style', 'css/change-style.css');
    } else {
        link.setAttribute('href', 'css/style.css');
        localStorage.setItem('style', 'css/style.css');
    }
    flag = !flag;
})
